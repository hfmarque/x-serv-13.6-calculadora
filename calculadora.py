#!/usr/bin/python3

import sys


def sumar(a, b):
    resultado: int = a + b
    return resultado


def restar(a, b):
    resultado: int = a - b
    return resultado


print("resultado :", sumar(1, 2))
print("resultado :", sumar(3, 4))

print("resultado :", restar(6, 5))
print("resultado :", restar(8, 7))

print(3)
